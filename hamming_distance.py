import sys
def hamming_distance(string1, string2):
    distance = 0
    strLen = len(string1)
    for i in range(strLen):
        if string1[i] != string2[i]:
            distance += 1
    return distance

def chosen_word(string1, string2):
    str1Len = len(string1)
    str2Len = len(string2)
    if str1Len != str2Len:
        return 0
    else:
        return 1

print('\n\nCalcul de distance de hamming entre deux mots.')
print('Veuillez entrer deux mots de même longeur pour avoir la distance de hamming entre eux.')
while True:
    mot1 = input("\n\nPremier mot : ")
    mot2 = input("Second mot : ")
    if chosen_word(mot1, mot2) == 0:
        print("{} et {} n'ont pas le même nombre de caractères. Veuillez réessayer".format(mot1, mot2))
        continue
    else:
        print('\n\nLa distance de hamming entre {} et {} est de {}'.format(mot1, mot2, hamming_distance(mot1, mot2)))
        res = None
        while res not in ("yes", "no"):
            res = input("\n\nVoulez-vous recommencer? y/n")
            if res == 'y':
                break
            if res == 'n':
                sys.exit()
            else:
                print('Valeur erroné, veuillez taper "y" pour oui ou "n" pour non')