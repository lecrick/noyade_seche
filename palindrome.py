import sys
def palindrome(string):
    return string == string[::-1]


print('-----Palindrome-----')
while True:
    mot = input("\n\nEntrez un mot pout tester si c'est un palindrome :\n")
    if palindrome(mot) == True:
        print("\n{} est un palindrome!".format(mot))
    else:
        print("\n{} n'est pas un palindrome car cela donne : {}".format(mot, mot[::-1]))
    res = None
    res = input("\n\nVoulez-vous recommencer? o/n\n")
    if res == 'o':
        continue
    if res == 'n':
        break
    else:
        print('valeur erroné, veuillez taper "o" pour oui ou "n" pour non')