firstCode = "CATA"
SecCode = "ATGC"

ADN = [[1,"Moutarde","CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGCADN"], [2,"Rose","CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGGADN"],[3,"Pervenche","AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCCADN"],[4,"Leblanc","CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"]]

def CompareDNA(adn, code1, code2):
    res1 = adn.find(code1)
    res2 = adn.find(code2)
    if res1 > 0 and res2 > 0:
        if abs(res1-res2) >= len(code1):
            return 1
    return -1

print("\n-----TEST ADN-----")
print("\nSuspect :\n1) Moutarde\n2) Rose\n3) Pervenche\n4) Leblanc")
print("\nPièces à conviction:\n1) 1er brin = CATA\n2) 2eme brin = ATGC")
res = input("\nDémarrer les test ADN? o/n")
if res == "o":
    for i in range(0, len(ADN)):
        if (CompareDNA(ADN[i][2], firstCode, SecCode)) == 1:
            print("\n{} est coupable!!!".format(ADN[i][1]))
